<?php

$last_saved_url = "";
function add_user_data($from_url, $link_type, $redirect_url){

	$last_saved_url = $from_url;

	$arr2 = json_decode(file_get_contents('userData.json'), true);
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') {
	    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip_address = $_SERVER['REMOTE_ADDR'];
	}

	$var  = array(
		'ip_address' => $ip_address,
		'link_type' => $link_type,
	);

	$arr2[] = $var;
	// print_r($arr2);
	file_put_contents("userData.json",json_encode($arr2));

	header('Location: http://' .  $_SERVER['HTTP_HOST'].'/busiman/'.$redirect_url);
    // die();
}

$link_type = $_GET["link_type"];
// echo $link_type;
add_user_data($_SERVER['SCRIPT_NAME'], $link_type, "index.php");
?> 
