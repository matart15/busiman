<?php

	include 'static_data.php';

	$book_id = $_GET["id"];
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>ビジネスマンガ</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/css/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 8]><script src="assets/css/ie/respond.min.js"></script><![endif]-->
</head>
<body class="right-sidebar">
	<div id="page-wrapper">

		<!-- Header -->
		<div id="header-wrapper">
			<header id="header" class="container">

				<!-- Logo -->
				<div id="logo">
					<h1><a href="index.php">BusiMan</a></h1>
					<span>マンガでわかる！</span>
				</div>

				<!-- Nav -->
				<nav id="nav">
					<ul>
						<li><a href="index.php"><i class="fa fa-home"></i>トップ</a></li>
						<li><a href="#footer"><i class="fa fa-tags"></i>カテゴリー</a></li>
						<li><a href="right-sidebar.php"><i class="fa fa-user-plus"></i>新規登録</a></li>
						<li><a href="no-sidebar.php"><i class="fa fa-leanpub"></i>ログイン</a></li>
					</ul>
				</nav>

			</header>
		</div>

		<!-- Main -->
		<div id="main-wrapper">
			<div class="container">
				<div class="row 200%">
					<div class="8u 12u$(medium)">
						<div id="content">

							<!-- Content -->
							<article>

								<h2>ログイン</h2>

								<p>無料でビジネス本がマンガで見れる！</p>

								<div align="center">
									<table border="0">
										<form action="index.php" method="get">
											<tr>
												<th>
													ユーザID
												</th>
												<td>
													<input type="text" name="user_id" value="" size="24">
												</td>
											</tr>
											<tr>
												<th>
													パスワード
												</th>
												<td>
													<input type="password" name="password" value="" size="24">
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<br />
													<div align="center"><input type="submit" value="ログイン"></div>
												</td>
											</tr>
										</form>
									</table>
								</div>

							</article>

						</div>
					</div>
					<div class="4u 12u$(medium)">
						<div id="sidebar">

							<!-- Sidebar -->
							<section class="widget contact">
								<h3>SNSログイン</h3>
								<p>下記のボタンからSNSと連携して会員登録する。</p>
								<ul>
									<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
									<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
								</ul>
								</section>

							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<div id="footer-wrapper">
				<footer id="footer" class="container">
					<div class="row">
						<div class="3u 6u(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<h3>本を探す</h3>
								<ul class="style2">
									<li><a href="#">キャリア・スキル・自己啓発</a></li>
									<li><a href="#">リーダーシップ・マネジメント</a></li>
									<li><a href="#">戦略</a></li>
									<li><a href="#">グローバル</a></li>
									<li><a href="#">起業</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u$(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<br />
								<ul class="style2">
									<li><a href="#">マーケティング</a></li>
									<li><a href="#">ファイナンス</a></li>
									<li><a href="#">IT</a></li>
									<li><a href="#">人事</a></li>
									<li><a href="#">オペレーション</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<br />
								<ul class="style2">
									<li><a href="#">政治・経済</a></li>
									<li><a href="#">産業・業界</a></li>
									<li><a href="#">コンセプト・トレンド</a></li>
									<li><a href="#">サイエンス・テクノロジー</a></li>
									<li><a href="#">人文科学</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u$(medium) 12u$(small)">

							<!-- Contact -->
							<section class="widget contact">
								<h3>Contact Us</h3>
								<ul>
									<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
									<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
									<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
								</ul>
								<p>1234 Fictional Road<br />
									Nashville, TN 00000<br />
									(800) 555-0000</p>
								</section>

							</div>
						</div>
						<div class="row">
							<div class="12u">
								<div id="copyright">
									<ul class="menu">
										<li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
				</div>

			</div>

			<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

		</body>
		</html>