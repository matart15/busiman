<?php
	include 'static_data.php';
	$x = "";
	if (isset($_GET["id"])) {
		if ($_GET["id"] == "register") {
			$x = "<h2>登録完了！！</h2>";
		}
	}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>ビジネスマンガ</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/css/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<script type="text/javascript" language="Javascript" src="assets/js/jquery.min.js"></script>
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 8]><script src="assets/css/ie/respond.min.js"></script><![endif]-->
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
			    type: "GET",
			    url: "get_user_data.php",
			    data: { link_type: document.URL },
			    success: function(data) {
					// $("#test").html(data);
	        		;
			    },
			    error: function(error) {
			console.log(error);
			    	;
			    }
			});

		    $('.my_counter_link').click(function () {
			    event.preventDefault();
			    console.log(event.target.nodeName);
			    var self = this;
				  $.ajax({
				    type: "GET",
				    url: "get_user_data.php",
				    data: { link_type: $(this).attr("href")},
				    success: function(data) {
		        		location.href = self.href;
				    },
				    error: function(error) {
				      alert(error);
				    }
				  });
			});
		});
	</script>
</head>
<body class="homepage">
<div id="test"></div>
	<div id="page-wrapper">

		<!-- Header -->
		<div id="header-wrapper">
			<header id="header" class="container">

				<!-- Logo -->

				<div id="logo">
					<h1><a href="index.php">BusiMan</a></h1>
					<span>マンガでわかる！</span>
				</div>

				<!-- Nav -->
				<nav id="nav">
					<ul>
						<li><a href="index.php"><i class="fa fa-home"></i>トップ</a></li>
						<li><a href="#footer"><i class="fa fa-tags"></i>カテゴリー</a></li>
						<li><a href="index.php?id=register"><i class="fa fa-user-plus"></i>新規登録</a></li>
						<li><a href="no-sidebar.php"><i class="fa fa-leanpub"></i>ログイン</a></li>
					</ul>
				</nav>
			</header>
		</div>
		<!-- Banner -->
		<div id="banner-wrapper">
			<div align='center'>
			<?php echo $x; ?>
	</div>
			<div id="banner" class="box container">
				<div class="row">
					<div class="7u 12u(medium)">
						<h2>分厚いビジネス書をさくっと楽しく読みたいアナタへ</h2>
						<p>話題のビジネス書が、マンガのダイジェスト版で読めるサービス<br /></p>
					</div>
					<div class="5u 12u(medium)">
						<div align="left">
					月に20冊まで無料でマンガのダイジェスト版が読める！！
				</div>
						<ul>
							<li><a href="index.php?id=register" class="button big icon fa-arrow-circle-right my_counter_link">→無料登録</a></li>
							<li><a href="#" class="button alt big icon fa-question-circle">→BusiMan</a></li>
						</ul>
					
					</div>
				</div>
			</div>
		</div>

		<!-- Features -->
		<div id="features-wrapper">
			<div class="container">
				
				<img src="images/dooon.png">
				<div class="newline" style="border-bottom:3px solid gray; font-size:40px; font-weight:bold; margin-bottom:20px; color: #0090c5; padding:20px;">【最新の本】</div>
				<div class="row">
					<?php for($i = 1; $i <= 3; $i++) : ?>
						<div class="4u 12u(medium)">
							<!-- Box -->
							<section class="box feature">
								<a href="left-sidebar.php?id=<?php echo $i?>" data-link_type="left" class="image featured my_counter_link"><img src=<?php echo '"images/'.$data_array[$i]['image_name'].'"'; ?>  alt="" /></a>
								<div class="inner">
									<header>
										<h2><?php echo $data_array[$i]['name']; ?> </h2>
									</header>
									<p>
								<a href="left-sidebar.php?id=<?php echo $i?>" data-link_type="left" class="my_counter_link">→マンガで読む(無料)</a>
									</p>
								</div>
							</section>
						</div>
					<?php endfor?>
				</div>
			</div>
		</div>

		<!-- Main -->
		<div id="main-wrapper">
			<div class="container">
				<div class="row 200%">
					<div class="4u 12u(medium)">

						<!-- Sidebar -->
						<div id="sidebar">
							<section class="widget thumbnails">
								<h3>他のビジネスマンガ</h3>
								<div class="grid">
									<div class="row 50%">
										<div class="6u"><a href="#" class="image fit"><img src="images/1m.png" alt="" /></a></div>
										<div class="6u"><a href="#" class="image fit"><img src="images/2m.png" alt="" /></a></div>
										<div class="6u"><a href="#" class="image fit"><img src="images/3m.png" alt="" /></a></div>
										<div class="6u"><a href="#" class="image fit"><img src="images/4m.png" alt="" /></a></div>
									</div>
								</div>
								<a href="#" class="button icon fa-file-text-o">もっと探す</a>
							</section>
						</div>

					</div>
					<div class="8u 12u(medium) important(medium)">

						<!-- Content -->
						<div id="content">
							<section class="last">
								<h2>他のマンガも見てみる？</h2>
								<p>他にも楽しくビジネスを学べるマンガが置いてあるので、
									ぜひ下のボタンをクリックしてマンガを探してみましょう！</p>
									<a href="#" class="button icon fa-arrow-circle-right">他のマンガも見てみる</a>
								</section>
							</div>

						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<div id="footer-wrapper">
				<footer id="footer" class="container">
					<div class="row">
						<div class="3u 6u(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<h3>本を探す</h3>
								<ul class="style2">
									<li><a href="#">キャリア・スキル・自己啓発</a></li>
									<li><a href="#">リーダーシップ・マネジメント</a></li>
									<li><a href="#">戦略</a></li>
									<li><a href="#">グローバル</a></li>
									<li><a href="#">起業</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u$(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<br />
								<ul class="style2">
									<li><a href="#">マーケティング</a></li>
									<li><a href="#">ファイナンス</a></li>
									<li><a href="#">IT</a></li>
									<li><a href="#">人事</a></li>
									<li><a href="#">オペレーション</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u(medium) 12u$(small)">

							<!-- Links -->
							<section class="widget links">
								<br />
								<ul class="style2">
									<li><a href="#">政治・経済</a></li>
									<li><a href="#">産業・業界</a></li>
									<li><a href="#">コンセプト・トレンド</a></li>
									<li><a href="#">サイエンス・テクノロジー</a></li>
									<li><a href="#">人文科学</a></li>
								</ul>
							</section>

						</div>
						<div class="3u 6u$(medium) 12u$(small)">

							<!-- Contact -->
							<section class="widget contact last">
								<h3>連絡先</h3>
								<ul>
									<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
									<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
									<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
								</ul>
								<p>1234 Fictional Road<br />
									Nashville, TN 00000<br />
									(800) 555-0000</p>
								</section>

							</div>
						</div>
						<div class="row">
							<div class="12u">
								<div id="copyright">
									<ul class="menu">
										<li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
				</div>

			</div>

			<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

		</body>
		</html>
