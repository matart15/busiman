<?php
	include 'static_data.php';

	$book_id = $_GET["id"];
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>ビジネスマンガ</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script type="text/javascript" language="Javascript" src="assets/js/jquery.min.js"></script>

	<!--[if lte IE 8]><script src="assets/css/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/fill.css" />
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 8]><script src="assets/css/ie/respond.min.js"></script><![endif]-->
	<style>
	.thumbnail {
		width: 125px;
		height: 83px;
		opacity: 0.5;
		cursor: pointer;
	}
	.current {
		opacity: 1.0;
	}
	.thumbnail + .thumbnail {
		margin-left: 4px;
	}
	html, body {
  		line-height: 1.5rem;
	}



	.book h1 p {
	  font-size: 1.3rem;
	}
	.btn-favorite {
	  font-size: 14px;
	  padding: 5px 10px;
	  height: 32px;
	}
	.book .detail {
	  border: 1px solid #ccc;
	  padding: 15px;
	  margin-top: 10px;
	}	
	.book-image {
	  position: relative;
	  float: left;
	}
	.book .fill {
	  float: right;
	  color: #191919;
	  margin-left: 20px;
	}
	.book .fill .fill-label {
	  float: left;
	  width: 100px;
	}
	button{
		font-size: 14px;
	}
	</style>
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
			    type: "GET",
			    url: "get_user_data.php",
			    data: { link_type: document.URL },
			    success: function(data) {
	        		;
			    },
			    error: function(error) {
			    	;
			    }
			});
		    $('.my_counter_link').click(function () {
			    event.preventDefault();
			    console.log(event.target.nodeName);
			    var self = this;
				  $.ajax({
				    type: "GET",
				    url: "get_user_data.php",
				    data: { link_type: $(this).attr("href")},
				    success: function(data) {
		        		location.href = self.href;
				    },
				    error: function(error) {
				      alert(error);
				    }
				  });
			});
		});
	</script>
</head>
<body class="left-sidebar">
	<div id="page-wrapper">

		<!-- Header -->
		<div id="header-wrapper">
			<header id="header" class="container">

				<!-- Logo -->
				<div id="logo">
					<h1><a href="index.php">BusiMan</a></h1>
					<span>マンガでわかる！</span>
				</div>

				<!-- Nav -->
				<nav id="nav">
					<ul>
						<li><a href="index.php"><i class="fa fa-home"></i>トップ</a></li>
						<li><a href="#footer"><i class="fa fa-tags"></i>カテゴリー</a></li>
						<li><a href="right-sidebar.php"><i class="fa fa-user-plus"></i>新規登録</a></li>
						<li><a href="no-sidebar.php"><i class="fa fa-leanpub"></i>ログイン</a></li>
					</ul>
				</nav>

			</header>
		</div>

		<!-- Main -->
		<div id="main-wrapper">
			<div class="container">
				<div class="row 200%">
					<div class="4u 12u$(medium)">
						<div id="sidebar">

							<!-- Sidebar -->
							<section>
								<h3>オススメマンガ</h3>
								<ul class="style2">
									<li><a href="#">明日のマネジメント</a></li>
									<li><a href="#">今日から始める3つの習慣</a></li>
								</ul>
								<br />
							</section>

						</div>
					</div>
					<div id="contents-main">
								<h2><?php echo $data_array[$book_id]["name"]; ?></h2>
							        	<br />

							            <img src=<?php echo '"images/'.$data_array[$book_id]['image_name'].'"'; ?>  class="cover">
							            <br />
		     							<p style="line-height: 2.5rem; margin-bottom: 0"><?php echo $data_array[$book_id]["name1"]; ?></p>
		     							<p>ジャンル:グローバル,起業,産業・業界,戦略<br />
		     							著者：大前 研一 監修、good.book<br />
		     							編集部 編集<br />
		     							ページ数：141ページ<br />
		     							出版社：good.book<br />
							            定価：1,620円<br />
							            出版日：2015年02月06日<br />
							        	</p>
		     							<p><?php echo $data_array[$book_id]["description"]; ?></p>
		     					<div style="background-color: #91DBB9; font-weight:bold; font-size:15px; margin-bottom:20px; padding:20px;!">
				                <a href="right-sidebar.php" class="pdf-download btn btn-success my_counter_link">
				                会員登録してマンガ(PDF)でダウンロード(無料)
				                <img src="images/pdf-download.png">
				            	</a>
				            </div>
				            	<br>
		     							<h3>↓購入はこちらからどうぞ↓</h3>
								        <div class="book" style="background-color:white;">
							            <a href="http://amzn.to/1O64CMY" class="shops amazon my_counter_link" data-shop="amazon" target="_blank"></a>
							            <a href="http://amzn.to/1O64CN3" class="shops kindle my_counter_link" data-shop="kindle" target="_blank"></a>
							            <a href="http://ck.jp.ap.valuecommerce.com/servlet/referral?vs=3105761&amp;vp=882453612&amp;xvg=6000299926&amp;va=2309713&amp;vc_url=http%3A%2F%2Fhonto.jp%2Febook%2Fpd_27147003.html" class="shops honto my_counter_link" data-shop="honto" target="_blank"></a>
							            <a href="http://ck.jp.ap.valuecommerce.com/servlet/referral?vs=3105761&amp;vp=882453612&amp;xvg=6000299926&amp;va=2542667&amp;vc_url=https%3A%2F%2Fwww.kinokuniya.co.jp%2Ff%2Fdsg-01-9784907554125" class="shops kinokuniya my_counter_link" data-shop="kinokuniya" target="_blank"></a> 
							        	</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Footer -->
		<div id="footer-wrapper">
			<footer id="footer" class="container">
				<div class="row">
					<div class="3u 6u(medium) 12u$(small)">

						<!-- Links -->
									<section class="widget links">
										<h3>本を探す</h3>
										<ul class="style2">
											<li><a href="#">キャリア・スキル・自己啓発</a></li>
											<li><a href="#">リーダーシップ・マネジメント</a></li>
											<li><a href="#">戦略</a></li>
											<li><a href="#">グローバル</a></li>
											<li><a href="#">起業</a></li>
										</ul>
									</section>

							</div>
							<div class="3u 6u$(medium) 12u$(small)">

								<!-- Links -->
									<section class="widget links">
										<br />
										<ul class="style2">
											<li><a href="#">マーケティング</a></li>
											<li><a href="#">ファイナンス</a></li>
											<li><a href="#">IT</a></li>
											<li><a href="#">人事</a></li>
											<li><a href="#">オペレーション</a></li>
										</ul>
									</section>

							</div>
							<div class="3u 6u(medium) 12u$(small)">

								<!-- Links -->
									<section class="widget links">
										<br />
										<ul class="style2">
											<li><a href="#">政治・経済</a></li>
											<li><a href="#">産業・業界</a></li>
											<li><a href="#">コンセプト・トレンド</a></li>
											<li><a href="#">サイエンス・テクノロジー</a></li>
											<li><a href="#">人文科学</a></li>
										</ul>
									</section>

					</div>
					<div class="3u 6u$(medium) 12u$(small)">

						<!-- Contact -->
						<section class="widget contact">
							<h3>Contact Us</h3>
							<ul>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
								<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
							</ul>
							<p>1234 Fictional Road<br />
								Nashville, TN 00000<br />
								(800) 555-0000</p>
							</section>

						</div>
					</div>
					<div class="row">
						<div class="12u">
							<div id="copyright">
								<ul class="menu">
									<li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
								</ul>
							</div>
						</div>
					</div>
				</footer>
			</div>

		</div>

		<!-- Scripts -->

		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>

	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
	$(function() {
		var files = [
		'images/1m.png',
		'images/2m.png',
		// 'images/3m.png',
		// 'images/4m.png',
		];
		var img;
		var timer;
		var currentNum = 0;
		var nowPlyaing = false;
		for (var i = 0; i < files.length; i++) {
			img = $('<img>').attr('src', files[i]).addClass('thumbnail');
			$('#thumbnails').append(img);
		}
		$('#mainnn').append(
			$('<img>').attr('src', files[0])	
			);
		$('.thumbnail:first').addClass('current');
		$('.thumbnail').click(function() {
			$('#mainnn img').attr('src', $(this).attr('src'));
			currentNum = $(this).index();
			$(this).addClass('current').siblings().removeClass('current');
		});
		$('#prev').click(function() {
			currentNum--;
			if (currentNum < 0) {
				currentNum = files.length -1;
			}
			$('#mainnn img').attr('src', files[currentNum]);
			$('.thumbnail').removeClass('current');
			$('.thumbnail').eq(currentNum).addClass('current');
		});
		$('#next').click(function() {
			currentNum++;
			if (currentNum > files.length -1) {
				currentNum = 0;
			}
			$('#mainnn img').attr('src', files[currentNum]);
			$('.thumbnail').removeClass('current');
			$('.thumbnail').eq(currentNum).addClass('current');
		});
		function autoPlay() {
			$('#next').click();
			timer = setTimeout(function() {
				autoPlay();
			}, 1000);
		}
		$('#play').click(function() {
			if (nowPlyaing) return;
			nowPlyaing = true;
			autoPlay();
		});
		$('#stop').click(function() {
			clearTimeout(timer);
			nowPlyaing = false;
		});
	});
</script>
</html>
